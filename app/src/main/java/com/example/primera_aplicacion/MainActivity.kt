package com.example.primera_aplicacion

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView

class MainActivity : AppCompatActivity() {

    private lateinit var welcomeMessageTextView : TextView //lateinit no utiliza memoria hasta que se asigne algo
    private lateinit var welcomeSubtitleTextView : TextView
    private lateinit var changeMessageTextButton : Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        welcomeMessageTextView = findViewById(R.id.welcome_message)
        welcomeSubtitleTextView = findViewById(R.id.main_message)
        changeMessageTextButton = findViewById(R.id.change_text_button)

        changeMessageTextButton.setOnClickListener{
            changeMessageAndSubtitles()
        }
    }

    private fun changeMessageAndSubtitles(){
        welcomeMessageTextView.text = "Hello Universe"
        welcomeSubtitleTextView.text = "Hey There!"
    }
}
